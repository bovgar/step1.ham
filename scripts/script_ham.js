//--------Section "Our Service"
const servTabsList = document.querySelectorAll(".services-tabs-list")[0]
servTabsList.addEventListener("click", function (event) {
    const servTabsItems = servTabsList.children;
    for (let i = 0; i < servTabsItems.length; i++) {
        servTabsItems[i].classList.remove("active");
    }
    console.log(event.target);
    event.target.classList.add("active");

    const servTabsContent = document.querySelectorAll(".services-tabs-content")
    servTabsContent.forEach(function (item) {
        item.classList.remove("active");
    });
    const contentId = event.target.dataset.tab;
    document.getElementById(contentId).classList.add("active");
});

//------Section "Our Amazing Work"
//------show images on load
const count = 12;
document.addEventListener("DOMContentLoaded", function () {
    document.querySelectorAll((`.work-type`)).forEach(item => item.style.display = "none");
    showImage("*", count);
});

//--------- select type of work
const workList = document.querySelector(".work-list");
workList.addEventListener("click", function (event) {
    loadWorkBtn.style.display = "inline-block";
    if (event.target.classList.contains("work-item")) {
        //----- add class "active" to selected work item
        document.querySelectorAll(".work-item").forEach(item => item.classList.remove("active"))
        event.target.classList.add("active");

        //----- show images, related to selected type of work
        document.querySelectorAll((`.work-type`)).forEach(item => item.style.display = "none");
        const selector = event.target.dataset.filter;
        console.log(`selector = ${selector}`);
        showImage(selector, count);
    }
});

//--------- push button "Load more"
const loadWorkBtn = document.querySelector(".work-load");
loadWorkBtn.addEventListener("click", function (event) {
    event.preventDefault();
    const selectedType = document.querySelector(".work-item.active").dataset.filter;
    console.log(`selectedType = ${selectedType}`);
    showImage(selectedType, count);
});

function showImage(selectorType, count = 12) {
    const pickedNode = document.querySelectorAll(`${selectorType}.work-type`);
    const hiddenNode = [...pickedNode].filter(item => item.style.display === "none");
    if (hiddenNode.length <= count) {
        loadWorkBtn.style.display = "none";
    }
    pickedArr = hiddenNode.splice(0, count);
    pickedArr.forEach(item => item.style.display = "block");
}

//------- create white div with content
const imgDiv = document.getElementsByClassName("work-type");
for (let i = 0; i < imgDiv.length; i++) {
    imgDiv[i].addEventListener("mouseenter", function (e) {
        const div = document.createElement("div");
        div.className = "work-creative";
        const category = this.dataset.category;
        const description = this.dataset.description;
        div.innerHTML = `                   
                        <div class="work-creative-content">
                        <div class="work-search">
                            <i class="fas fa-link portfolio-awesome portfolio-link"></i>
                            <i class="fas fa-search portfolio-awesome portfolio-search"></i>
                        </div>
                        <p class="portfolio-category-name">${description}</p>
                        <p class="portfolio-item-description">${category}</p>
                        </div>
                    `;
        this.prepend(div)
    });
    imgDiv[i].addEventListener("mouseleave", function () {
        this.querySelector(".work-creative").remove()
    })
}

//SECTION - Breaking News
document.querySelectorAll(".news-link").forEach(item => item.addEventListener("click", evt => (evt.preventDefault()) ));
